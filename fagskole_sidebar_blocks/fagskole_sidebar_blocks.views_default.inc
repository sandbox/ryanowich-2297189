<?php
/**
 * @file
 * fagskole_sidebar_blocks.views_default.inc
 */

/**
 * Implements hook_views_default_views().
 */
function fagskole_sidebar_blocks_views_default_views() {
  $export = array();

  $view = new view();
  $view->name = 'latest_images_and_videos';
  $view->description = '';
  $view->tag = 'default';
  $view->base_table = 'node';
  $view->human_name = 'Latest: Images and videos';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['title'] = 'Sidste nyt';
  $handler->display->display_options['css_class'] = 'sidebar_latest clearfix';
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'perm';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'some';
  $handler->display->display_options['pager']['options']['items_per_page'] = '10';
  $handler->display->display_options['style_plugin'] = 'default';
  $handler->display->display_options['style_options']['row_class'] = 'latest_imagevideo';
  $handler->display->display_options['style_options']['default_row_class'] = FALSE;
  $handler->display->display_options['row_plugin'] = 'fields';
  $handler->display->display_options['row_options']['inline'] = array(
    'field_gallery_image' => 'field_gallery_image',
    'every_field_image' => 'every_field_image',
    'field_image_1' => 'field_image_1',
  );
  $handler->display->display_options['row_options']['hide_empty'] = TRUE;
  /* Field: Content: Title */
  $handler->display->display_options['fields']['title']['id'] = 'title';
  $handler->display->display_options['fields']['title']['table'] = 'node';
  $handler->display->display_options['fields']['title']['field'] = 'title';
  $handler->display->display_options['fields']['title']['label'] = '';
  $handler->display->display_options['fields']['title']['alter']['word_boundary'] = FALSE;
  $handler->display->display_options['fields']['title']['alter']['ellipsis'] = FALSE;
  /* Sort criterion: Content: Post date */
  $handler->display->display_options['sorts']['created']['id'] = 'created';
  $handler->display->display_options['sorts']['created']['table'] = 'node';
  $handler->display->display_options['sorts']['created']['field'] = 'created';
  $handler->display->display_options['sorts']['created']['order'] = 'DESC';
  $handler->display->display_options['filter_groups']['groups'] = array(
    1 => 'AND',
    2 => 'OR',
  );
  /* Filter criterion: Content: Published */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'node';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = 1;
  $handler->display->display_options['filters']['status']['group'] = 1;
  $handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
  /* Filter criterion: Content: Type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'node';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'article' => 'article',
    'gallery' => 'gallery',
  );
  $handler->display->display_options['filters']['type']['group'] = 1;
  /* Filter criterion: Content: Main Gallery Image (field_gallery_image:fid) */
  $handler->display->display_options['filters']['field_gallery_image_fid']['id'] = 'field_gallery_image_fid';
  $handler->display->display_options['filters']['field_gallery_image_fid']['table'] = 'field_data_field_gallery_image';
  $handler->display->display_options['filters']['field_gallery_image_fid']['field'] = 'field_gallery_image_fid';
  $handler->display->display_options['filters']['field_gallery_image_fid']['operator'] = 'not empty';
  $handler->display->display_options['filters']['field_gallery_image_fid']['group'] = 2;
  /* Filter criterion: Content: Upload an image (every_field_image:fid) */
  $handler->display->display_options['filters']['every_field_image_fid']['id'] = 'every_field_image_fid';
  $handler->display->display_options['filters']['every_field_image_fid']['table'] = 'field_data_every_field_image';
  $handler->display->display_options['filters']['every_field_image_fid']['field'] = 'every_field_image_fid';
  $handler->display->display_options['filters']['every_field_image_fid']['operator'] = 'not empty';
  $handler->display->display_options['filters']['every_field_image_fid']['group'] = 2;
  /* Filter criterion: Field: Foto (field_image:fid) */
  $handler->display->display_options['filters']['field_image_fid']['id'] = 'field_image_fid';
  $handler->display->display_options['filters']['field_image_fid']['table'] = 'field_data_field_image';
  $handler->display->display_options['filters']['field_image_fid']['field'] = 'field_image_fid';
  $handler->display->display_options['filters']['field_image_fid']['operator'] = 'not empty';
  $handler->display->display_options['filters']['field_image_fid']['group'] = 2;

  /* Display: Latest: Images and videos */
  $handler = $view->new_display('block', 'Latest: Images and videos', 'latest_img_video_all');
  $handler->display->display_options['defaults']['pager'] = FALSE;
  $handler->display->display_options['pager']['type'] = 'some';
  $handler->display->display_options['pager']['options']['items_per_page'] = '8';
  $handler->display->display_options['pager']['options']['offset'] = '0';
  $handler->display->display_options['defaults']['fields'] = FALSE;
  /* Field: Content: Main Gallery Image */
  $handler->display->display_options['fields']['field_gallery_image']['id'] = 'field_gallery_image';
  $handler->display->display_options['fields']['field_gallery_image']['table'] = 'field_data_field_gallery_image';
  $handler->display->display_options['fields']['field_gallery_image']['field'] = 'field_gallery_image';
  $handler->display->display_options['fields']['field_gallery_image']['label'] = '';
  $handler->display->display_options['fields']['field_gallery_image']['alter']['text'] = '<div class="bgimage" style="background-image:url([field_gallery_image]);"></div>';
  $handler->display->display_options['fields']['field_gallery_image']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_gallery_image']['hide_empty'] = TRUE;
  $handler->display->display_options['fields']['field_gallery_image']['click_sort_column'] = 'fid';
  $handler->display->display_options['fields']['field_gallery_image']['settings'] = array(
    'image_style' => 'sidebar_half',
    'image_link' => 'content',
  );
  /* Field: Content: Upload an image */
  $handler->display->display_options['fields']['every_field_image']['id'] = 'every_field_image';
  $handler->display->display_options['fields']['every_field_image']['table'] = 'field_data_every_field_image';
  $handler->display->display_options['fields']['every_field_image']['field'] = 'every_field_image';
  $handler->display->display_options['fields']['every_field_image']['label'] = '';
  $handler->display->display_options['fields']['every_field_image']['alter']['text'] = '<div class="bgimage" style="background-image:url([every_field_image]);"></div>';
  $handler->display->display_options['fields']['every_field_image']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['every_field_image']['hide_empty'] = TRUE;
  $handler->display->display_options['fields']['every_field_image']['click_sort_column'] = 'fid';
  $handler->display->display_options['fields']['every_field_image']['settings'] = array(
    'image_style' => 'sidebar_half',
    'image_link' => 'content',
  );
  /* Field: Field: Foto */
  $handler->display->display_options['fields']['field_image_1']['id'] = 'field_image_1';
  $handler->display->display_options['fields']['field_image_1']['table'] = 'field_data_field_image';
  $handler->display->display_options['fields']['field_image_1']['field'] = 'field_image';
  $handler->display->display_options['fields']['field_image_1']['label'] = '';
  $handler->display->display_options['fields']['field_image_1']['alter']['text'] = '<div class="bgimage" style="background-image:url([field_image_1]);"></div>';
  $handler->display->display_options['fields']['field_image_1']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_image_1']['hide_empty'] = TRUE;
  $handler->display->display_options['fields']['field_image_1']['click_sort_column'] = 'fid';
  $handler->display->display_options['fields']['field_image_1']['settings'] = array(
    'image_style' => 'sidebar_half',
    'image_link' => 'content',
  );
  /* Field: Content: Post date full (medium) */
  $handler->display->display_options['fields']['created']['id'] = 'created';
  $handler->display->display_options['fields']['created']['table'] = 'node';
  $handler->display->display_options['fields']['created']['field'] = 'created';
  $handler->display->display_options['fields']['created']['ui_name'] = 'Content: Post date full (medium)';
  $handler->display->display_options['fields']['created']['label'] = '';
  $handler->display->display_options['fields']['created']['element_type'] = 'div';
  $handler->display->display_options['fields']['created']['element_class'] = 'post_date_medium';
  $handler->display->display_options['fields']['created']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['created']['element_default_classes'] = FALSE;
  $handler->display->display_options['fields']['created']['date_format'] = 'medium';
  $handler->display->display_options['fields']['created']['second_date_format'] = 'long';
  $handler->display->display_options['fields']['created']['timezone'] = 'Europe/Copenhagen';
  /* Field: Content: Post date day (leading zero) */
  $handler->display->display_options['fields']['created_1']['id'] = 'created_1';
  $handler->display->display_options['fields']['created_1']['table'] = 'node';
  $handler->display->display_options['fields']['created_1']['field'] = 'created';
  $handler->display->display_options['fields']['created_1']['ui_name'] = 'Content: Post date day (leading zero)';
  $handler->display->display_options['fields']['created_1']['label'] = '';
  $handler->display->display_options['fields']['created_1']['exclude'] = TRUE;
  $handler->display->display_options['fields']['created_1']['element_type'] = 'span';
  $handler->display->display_options['fields']['created_1']['element_class'] = 'day';
  $handler->display->display_options['fields']['created_1']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['created_1']['element_default_classes'] = FALSE;
  $handler->display->display_options['fields']['created_1']['date_format'] = 'custom';
  $handler->display->display_options['fields']['created_1']['custom_date_format'] = 'd';
  $handler->display->display_options['fields']['created_1']['second_date_format'] = 'long';
  $handler->display->display_options['fields']['created_1']['timezone'] = 'Europe/Copenhagen';
  /* Field: Content: Post date month (3 letters) */
  $handler->display->display_options['fields']['created_2']['id'] = 'created_2';
  $handler->display->display_options['fields']['created_2']['table'] = 'node';
  $handler->display->display_options['fields']['created_2']['field'] = 'created';
  $handler->display->display_options['fields']['created_2']['ui_name'] = 'Content: Post date month (3 letters)';
  $handler->display->display_options['fields']['created_2']['label'] = '';
  $handler->display->display_options['fields']['created_2']['exclude'] = TRUE;
  $handler->display->display_options['fields']['created_2']['element_type'] = 'span';
  $handler->display->display_options['fields']['created_2']['element_class'] = 'month';
  $handler->display->display_options['fields']['created_2']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['created_2']['element_default_classes'] = FALSE;
  $handler->display->display_options['fields']['created_2']['date_format'] = 'custom';
  $handler->display->display_options['fields']['created_2']['custom_date_format'] = 'M';
  $handler->display->display_options['fields']['created_2']['second_date_format'] = 'long';
  $handler->display->display_options['fields']['created_2']['timezone'] = 'Europe/Copenhagen';
  /* Field: Content: Post date year */
  $handler->display->display_options['fields']['created_3']['id'] = 'created_3';
  $handler->display->display_options['fields']['created_3']['table'] = 'node';
  $handler->display->display_options['fields']['created_3']['field'] = 'created';
  $handler->display->display_options['fields']['created_3']['ui_name'] = 'Content: Post date year';
  $handler->display->display_options['fields']['created_3']['label'] = '';
  $handler->display->display_options['fields']['created_3']['exclude'] = TRUE;
  $handler->display->display_options['fields']['created_3']['element_type'] = 'span';
  $handler->display->display_options['fields']['created_3']['element_class'] = 'year';
  $handler->display->display_options['fields']['created_3']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['created_3']['element_default_classes'] = FALSE;
  $handler->display->display_options['fields']['created_3']['date_format'] = 'custom';
  $handler->display->display_options['fields']['created_3']['custom_date_format'] = 'Y';
  $handler->display->display_options['fields']['created_3']['second_date_format'] = 'long';
  $handler->display->display_options['fields']['created_3']['timezone'] = 'Europe/Copenhagen';
  /* Field: Content: Post date GFX span */
  $handler->display->display_options['fields']['counter']['id'] = 'counter';
  $handler->display->display_options['fields']['counter']['table'] = 'views';
  $handler->display->display_options['fields']['counter']['field'] = 'counter';
  $handler->display->display_options['fields']['counter']['ui_name'] = 'Content: Post date GFX span';
  $handler->display->display_options['fields']['counter']['label'] = '';
  $handler->display->display_options['fields']['counter']['alter']['alter_text'] = TRUE;
  $handler->display->display_options['fields']['counter']['alter']['text'] = '<span class="day">[created_1]</span>
<span class="month_year">
	<span class="month">[created_2]</span>
	<span class="year">[created_3]</span>
</span>';
  $handler->display->display_options['fields']['counter']['element_type'] = 'div';
  $handler->display->display_options['fields']['counter']['element_class'] = 'post_date_gfx';
  $handler->display->display_options['fields']['counter']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['counter']['element_default_classes'] = FALSE;
  $handler->display->display_options['fields']['counter']['counter_start'] = '1';
  /* Field: Content: Title */
  $handler->display->display_options['fields']['title']['id'] = 'title';
  $handler->display->display_options['fields']['title']['table'] = 'node';
  $handler->display->display_options['fields']['title']['field'] = 'title';
  $handler->display->display_options['fields']['title']['label'] = '';
  $handler->display->display_options['fields']['title']['alter']['word_boundary'] = FALSE;
  $handler->display->display_options['fields']['title']['alter']['ellipsis'] = FALSE;
  $handler->display->display_options['fields']['title']['element_type'] = 'h5';
  $handler->display->display_options['fields']['title']['element_class'] = 'latest_title';
  $handler->display->display_options['fields']['title']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['title']['hide_empty'] = TRUE;
  $handler->display->display_options['block_description'] = 'Latest: Images and videos';

  /* Display: Latest: Images and videos (by taxonomy) */
  $handler = $view->new_display('block', 'Latest: Images and videos (by taxonomy)', 'latest_img_video_tax');
  $handler->display->display_options['defaults']['css_class'] = FALSE;
  $handler->display->display_options['css_class'] = 'sidebar_latest bytax clearfix';
  $handler->display->display_options['defaults']['pager'] = FALSE;
  $handler->display->display_options['pager']['type'] = 'some';
  $handler->display->display_options['pager']['options']['items_per_page'] = '80';
  $handler->display->display_options['pager']['options']['offset'] = '0';
  $handler->display->display_options['defaults']['relationships'] = FALSE;
  /* Relationship: Content: Taxonomy terms on node */
  $handler->display->display_options['relationships']['term_node_tid']['id'] = 'term_node_tid';
  $handler->display->display_options['relationships']['term_node_tid']['table'] = 'node';
  $handler->display->display_options['relationships']['term_node_tid']['field'] = 'term_node_tid';
  $handler->display->display_options['relationships']['term_node_tid']['label'] = 'taxonomy';
  $handler->display->display_options['relationships']['term_node_tid']['required'] = TRUE;
  $handler->display->display_options['relationships']['term_node_tid']['vocabularies'] = array(
    'faglinjer' => 'faglinjer',
    'menu_emner' => 0,
  );
  $handler->display->display_options['defaults']['fields'] = FALSE;
  /* Field: Content: Main Gallery Image */
  $handler->display->display_options['fields']['field_gallery_image']['id'] = 'field_gallery_image';
  $handler->display->display_options['fields']['field_gallery_image']['table'] = 'field_data_field_gallery_image';
  $handler->display->display_options['fields']['field_gallery_image']['field'] = 'field_gallery_image';
  $handler->display->display_options['fields']['field_gallery_image']['label'] = '';
  $handler->display->display_options['fields']['field_gallery_image']['alter']['text'] = '<div class="bgimage" style="background-image:url([field_gallery_image]);"></div>';
  $handler->display->display_options['fields']['field_gallery_image']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_gallery_image']['hide_empty'] = TRUE;
  $handler->display->display_options['fields']['field_gallery_image']['click_sort_column'] = 'fid';
  $handler->display->display_options['fields']['field_gallery_image']['settings'] = array(
    'image_style' => 'sidebar_half',
    'image_link' => 'content',
  );
  /* Field: Content: Upload an image */
  $handler->display->display_options['fields']['every_field_image']['id'] = 'every_field_image';
  $handler->display->display_options['fields']['every_field_image']['table'] = 'field_data_every_field_image';
  $handler->display->display_options['fields']['every_field_image']['field'] = 'every_field_image';
  $handler->display->display_options['fields']['every_field_image']['label'] = '';
  $handler->display->display_options['fields']['every_field_image']['alter']['text'] = '<div class="bgimage" style="background-image:url([every_field_image]);"></div>';
  $handler->display->display_options['fields']['every_field_image']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['every_field_image']['hide_empty'] = TRUE;
  $handler->display->display_options['fields']['every_field_image']['click_sort_column'] = 'fid';
  $handler->display->display_options['fields']['every_field_image']['settings'] = array(
    'image_style' => 'sidebar_half',
    'image_link' => 'content',
  );
  /* Field: Field: Foto */
  $handler->display->display_options['fields']['field_image_1']['id'] = 'field_image_1';
  $handler->display->display_options['fields']['field_image_1']['table'] = 'field_data_field_image';
  $handler->display->display_options['fields']['field_image_1']['field'] = 'field_image';
  $handler->display->display_options['fields']['field_image_1']['label'] = '';
  $handler->display->display_options['fields']['field_image_1']['alter']['text'] = '<div class="bgimage" style="background-image:url([field_image_1]);"></div>';
  $handler->display->display_options['fields']['field_image_1']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_image_1']['hide_empty'] = TRUE;
  $handler->display->display_options['fields']['field_image_1']['click_sort_column'] = 'fid';
  $handler->display->display_options['fields']['field_image_1']['settings'] = array(
    'image_style' => 'sidebar_half',
    'image_link' => 'content',
  );
  /* Field: Content: Post date full (medium) */
  $handler->display->display_options['fields']['created']['id'] = 'created';
  $handler->display->display_options['fields']['created']['table'] = 'node';
  $handler->display->display_options['fields']['created']['field'] = 'created';
  $handler->display->display_options['fields']['created']['ui_name'] = 'Content: Post date full (medium)';
  $handler->display->display_options['fields']['created']['label'] = '';
  $handler->display->display_options['fields']['created']['element_type'] = 'div';
  $handler->display->display_options['fields']['created']['element_class'] = 'post_date_medium';
  $handler->display->display_options['fields']['created']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['created']['element_default_classes'] = FALSE;
  $handler->display->display_options['fields']['created']['date_format'] = 'medium';
  $handler->display->display_options['fields']['created']['second_date_format'] = 'long';
  $handler->display->display_options['fields']['created']['timezone'] = 'Europe/Copenhagen';
  /* Field: Content: Post date day (leading zero) */
  $handler->display->display_options['fields']['created_1']['id'] = 'created_1';
  $handler->display->display_options['fields']['created_1']['table'] = 'node';
  $handler->display->display_options['fields']['created_1']['field'] = 'created';
  $handler->display->display_options['fields']['created_1']['ui_name'] = 'Content: Post date day (leading zero)';
  $handler->display->display_options['fields']['created_1']['label'] = '';
  $handler->display->display_options['fields']['created_1']['exclude'] = TRUE;
  $handler->display->display_options['fields']['created_1']['element_type'] = 'span';
  $handler->display->display_options['fields']['created_1']['element_class'] = 'day';
  $handler->display->display_options['fields']['created_1']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['created_1']['element_default_classes'] = FALSE;
  $handler->display->display_options['fields']['created_1']['date_format'] = 'custom';
  $handler->display->display_options['fields']['created_1']['custom_date_format'] = 'd';
  $handler->display->display_options['fields']['created_1']['second_date_format'] = 'long';
  $handler->display->display_options['fields']['created_1']['timezone'] = 'Europe/Copenhagen';
  /* Field: Content: Post date month (3 letters) */
  $handler->display->display_options['fields']['created_2']['id'] = 'created_2';
  $handler->display->display_options['fields']['created_2']['table'] = 'node';
  $handler->display->display_options['fields']['created_2']['field'] = 'created';
  $handler->display->display_options['fields']['created_2']['ui_name'] = 'Content: Post date month (3 letters)';
  $handler->display->display_options['fields']['created_2']['label'] = '';
  $handler->display->display_options['fields']['created_2']['exclude'] = TRUE;
  $handler->display->display_options['fields']['created_2']['element_type'] = 'span';
  $handler->display->display_options['fields']['created_2']['element_class'] = 'month';
  $handler->display->display_options['fields']['created_2']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['created_2']['element_default_classes'] = FALSE;
  $handler->display->display_options['fields']['created_2']['date_format'] = 'custom';
  $handler->display->display_options['fields']['created_2']['custom_date_format'] = 'M';
  $handler->display->display_options['fields']['created_2']['second_date_format'] = 'long';
  $handler->display->display_options['fields']['created_2']['timezone'] = 'Europe/Copenhagen';
  /* Field: Content: Post date year */
  $handler->display->display_options['fields']['created_3']['id'] = 'created_3';
  $handler->display->display_options['fields']['created_3']['table'] = 'node';
  $handler->display->display_options['fields']['created_3']['field'] = 'created';
  $handler->display->display_options['fields']['created_3']['ui_name'] = 'Content: Post date year';
  $handler->display->display_options['fields']['created_3']['label'] = '';
  $handler->display->display_options['fields']['created_3']['exclude'] = TRUE;
  $handler->display->display_options['fields']['created_3']['element_type'] = 'span';
  $handler->display->display_options['fields']['created_3']['element_class'] = 'year';
  $handler->display->display_options['fields']['created_3']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['created_3']['element_default_classes'] = FALSE;
  $handler->display->display_options['fields']['created_3']['date_format'] = 'custom';
  $handler->display->display_options['fields']['created_3']['custom_date_format'] = 'Y';
  $handler->display->display_options['fields']['created_3']['second_date_format'] = 'long';
  $handler->display->display_options['fields']['created_3']['timezone'] = 'Europe/Copenhagen';
  /* Field: Content: Post date GFX span */
  $handler->display->display_options['fields']['counter']['id'] = 'counter';
  $handler->display->display_options['fields']['counter']['table'] = 'views';
  $handler->display->display_options['fields']['counter']['field'] = 'counter';
  $handler->display->display_options['fields']['counter']['ui_name'] = 'Content: Post date GFX span';
  $handler->display->display_options['fields']['counter']['label'] = '';
  $handler->display->display_options['fields']['counter']['alter']['alter_text'] = TRUE;
  $handler->display->display_options['fields']['counter']['alter']['text'] = '<span class="day">[created_1]</span>
<span class="month_year">
	<span class="month">[created_2]</span>
	<span class="year">[created_3]</span>
</span>';
  $handler->display->display_options['fields']['counter']['element_type'] = 'div';
  $handler->display->display_options['fields']['counter']['element_class'] = 'post_date_gfx';
  $handler->display->display_options['fields']['counter']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['counter']['element_default_classes'] = FALSE;
  $handler->display->display_options['fields']['counter']['counter_start'] = '1';
  /* Field: Content: Title */
  $handler->display->display_options['fields']['title']['id'] = 'title';
  $handler->display->display_options['fields']['title']['table'] = 'node';
  $handler->display->display_options['fields']['title']['field'] = 'title';
  $handler->display->display_options['fields']['title']['label'] = '';
  $handler->display->display_options['fields']['title']['alter']['word_boundary'] = FALSE;
  $handler->display->display_options['fields']['title']['alter']['ellipsis'] = FALSE;
  $handler->display->display_options['fields']['title']['element_type'] = 'h5';
  $handler->display->display_options['fields']['title']['element_class'] = 'latest_title';
  $handler->display->display_options['fields']['title']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['title']['hide_empty'] = TRUE;
  $handler->display->display_options['defaults']['arguments'] = FALSE;
  /* Contextual filter: Content: Has taxonomy term ID (with depth) */
  $handler->display->display_options['arguments']['term_node_tid_depth']['id'] = 'term_node_tid_depth';
  $handler->display->display_options['arguments']['term_node_tid_depth']['table'] = 'node';
  $handler->display->display_options['arguments']['term_node_tid_depth']['field'] = 'term_node_tid_depth';
  $handler->display->display_options['arguments']['term_node_tid_depth']['default_action'] = 'default';
  $handler->display->display_options['arguments']['term_node_tid_depth']['default_argument_type'] = 'taxonomy_tid';
  $handler->display->display_options['arguments']['term_node_tid_depth']['default_argument_options']['node'] = TRUE;
  $handler->display->display_options['arguments']['term_node_tid_depth']['default_argument_options']['anyall'] = '+';
  $handler->display->display_options['arguments']['term_node_tid_depth']['default_argument_options']['limit'] = TRUE;
  $handler->display->display_options['arguments']['term_node_tid_depth']['default_argument_options']['vocabularies'] = array(
    'faglinjer' => 'faglinjer',
  );
  $handler->display->display_options['arguments']['term_node_tid_depth']['summary']['number_of_records'] = '0';
  $handler->display->display_options['arguments']['term_node_tid_depth']['summary']['format'] = 'default_summary';
  $handler->display->display_options['arguments']['term_node_tid_depth']['summary_options']['items_per_page'] = '25';
  $handler->display->display_options['arguments']['term_node_tid_depth']['depth'] = '0';
  $handler->display->display_options['defaults']['filter_groups'] = FALSE;
  $handler->display->display_options['filter_groups']['groups'] = array(
    1 => 'AND',
    2 => 'OR',
  );
  $handler->display->display_options['defaults']['filters'] = FALSE;
  /* Filter criterion: Content: Published */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'node';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = 1;
  $handler->display->display_options['filters']['status']['group'] = 1;
  $handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
  /* Filter criterion: Content: Type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'node';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'article' => 'article',
    'gallery' => 'gallery',
  );
  $handler->display->display_options['filters']['type']['group'] = 1;
  /* Filter criterion: Content: Main Gallery Image (field_gallery_image:fid) */
  $handler->display->display_options['filters']['field_gallery_image_fid']['id'] = 'field_gallery_image_fid';
  $handler->display->display_options['filters']['field_gallery_image_fid']['table'] = 'field_data_field_gallery_image';
  $handler->display->display_options['filters']['field_gallery_image_fid']['field'] = 'field_gallery_image_fid';
  $handler->display->display_options['filters']['field_gallery_image_fid']['operator'] = 'not empty';
  $handler->display->display_options['filters']['field_gallery_image_fid']['group'] = 2;
  /* Filter criterion: Content: Upload an image (every_field_image:fid) */
  $handler->display->display_options['filters']['every_field_image_fid']['id'] = 'every_field_image_fid';
  $handler->display->display_options['filters']['every_field_image_fid']['table'] = 'field_data_every_field_image';
  $handler->display->display_options['filters']['every_field_image_fid']['field'] = 'every_field_image_fid';
  $handler->display->display_options['filters']['every_field_image_fid']['operator'] = 'not empty';
  $handler->display->display_options['filters']['every_field_image_fid']['group'] = 2;
  /* Filter criterion: Field: Foto (field_image:fid) */
  $handler->display->display_options['filters']['field_image_fid']['id'] = 'field_image_fid';
  $handler->display->display_options['filters']['field_image_fid']['table'] = 'field_data_field_image';
  $handler->display->display_options['filters']['field_image_fid']['field'] = 'field_image_fid';
  $handler->display->display_options['filters']['field_image_fid']['operator'] = 'not empty';
  $handler->display->display_options['filters']['field_image_fid']['group'] = 2;
  $handler->display->display_options['block_description'] = 'Latest: Images and videos (by tax)';
  $translatables['latest_images_and_videos'] = array(
    t('Master'),
    t('Sidste nyt'),
    t('more'),
    t('Apply'),
    t('Reset'),
    t('Sort by'),
    t('Asc'),
    t('Desc'),
    t('Latest: Images and videos'),
    t('<div class="bgimage" style="background-image:url([field_gallery_image]);"></div>'),
    t('<div class="bgimage" style="background-image:url([every_field_image]);"></div>'),
    t('<div class="bgimage" style="background-image:url([field_image_1]);"></div>'),
    t('<span class="day">[created_1]</span>
<span class="month_year">
	<span class="month">[created_2]</span>
	<span class="year">[created_3]</span>
</span>'),
    t('Latest: Images and videos (by taxonomy)'),
    t('taxonomy'),
    t('All'),
    t('Latest: Images and videos (by tax)'),
  );
  $export['latest_images_and_videos'] = $view;

  return $export;
}
