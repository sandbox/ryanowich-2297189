<?php
/**
 * @file
 * fagskole_sidebar_blocks.features.inc
 */

/**
 * Implements hook_views_api().
 */
function fagskole_sidebar_blocks_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}
