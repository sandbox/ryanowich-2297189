<?php
/**
 * @file
 * fagskole_sidebar_blocks.features.fe_block_boxes.inc
 */

/**
 * Implements hook_default_fe_block_boxes().
 */
function fagskole_sidebar_blocks_default_fe_block_boxes() {
  $export = array();

  $fe_block_boxes = new stdClass();
  $fe_block_boxes->info = 'Sub-taxonomy terms';
  $fe_block_boxes->format = 'php_code';
  $fe_block_boxes->machine_name = 'sub_tax_terms';
  $fe_block_boxes->body = '<?php // $vid is the vocabulary id.
    print gladfagskole_faglinjerchild_terms($vid = 1);
?>';

  $export['sub_tax_terms'] = $fe_block_boxes;

  return $export;
}
