<?php
/**
 * @file
 * fagskole_sidebar_blocks.features.fe_block_settings.inc
 */

/**
 * Implements hook_default_fe_block_settings().
 */
function fagskole_sidebar_blocks_default_fe_block_settings() {
  $export = array();

  $export['version'] = '2.0';

  $export['block-sub_tax_terms'] = array(
    'cache' => -1,
    'css_class' => '',
    'custom' => 0,
    'machine_name' => 'sub_tax_terms',
    'module' => 'block',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(),
    'themes' => array(
      'gladfagskole' => array(
        'region' => 'sidebar_right',
        'status' => 1,
        'theme' => 'gladfagskole',
        'weight' => -15,
      ),
    ),
    'title' => 'Downloads',
    'visibility' => 0,
  );

  $export['search-form'] = array(
    'cache' => -1,
    'css_class' => '',
    'custom' => 0,
    'delta' => 'form',
    'module' => 'search',
    'node_types' => array(),
    'pages' => 'contact',
    'roles' => array(),
    'themes' => array(
      'gladfagskole' => array(
        'region' => 'sidebar_right',
        'status' => 1,
        'theme' => 'gladfagskole',
        'weight' => -11,
      ),
    ),
    'title' => '',
    'visibility' => 0,
  );

  $export['user-login'] = array(
    'cache' => -1,
    'css_class' => '',
    'custom' => 0,
    'delta' => 'login',
    'module' => 'user',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(
      'authenticated user' => 2,
    ),
    'themes' => array(
      'gladfagskole' => array(
        'region' => 'sidebar_right',
        'status' => 1,
        'theme' => 'gladfagskole',
        'weight' => -12,
      ),
    ),
    'title' => '',
    'visibility' => 0,
  );

  $export['views-955221e31ed56270b797470df96ed9d7'] = array(
    'cache' => -1,
    'css_class' => '',
    'custom' => 0,
    'delta' => '955221e31ed56270b797470df96ed9d7',
    'module' => 'views',
    'node_types' => array(),
    'pages' => 'faglinjer
faglinier
faglinjer/*
faglinier/*',
    'roles' => array(),
    'themes' => array(
      'gladfagskole' => array(
        'region' => 'sidebar_right',
        'status' => 1,
        'theme' => 'gladfagskole',
        'weight' => -14,
      ),
    ),
    'title' => 'Sidste nyt',
    'visibility' => 1,
  );

  $export['views-bac9eff0b003b43b4f5304fe4c8d3d09'] = array(
    'cache' => -1,
    'css_class' => '',
    'custom' => 0,
    'delta' => 'bac9eff0b003b43b4f5304fe4c8d3d09',
    'module' => 'views',
    'node_types' => array(),
    'pages' => 'faglinjer
faglinier
faglinjer/*
faglinier/*',
    'roles' => array(),
    'themes' => array(
      'gladfagskole' => array(
        'region' => 'sidebar_right',
        'status' => 1,
        'theme' => 'gladfagskole',
        'weight' => -13,
      ),
    ),
    'title' => 'Sidste nyt',
    'visibility' => 0,
  );

  return $export;
}
