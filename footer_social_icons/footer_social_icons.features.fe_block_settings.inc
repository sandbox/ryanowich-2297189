<?php
/**
 * @file
 * footer_social_icons.features.fe_block_settings.inc
 */

/**
 * Implements hook_default_fe_block_settings().
 */
function footer_social_icons_default_fe_block_settings() {
  $export = array();

  $export['version'] = '2.0';

  $export['block-social_icons'] = array(
    'cache' => -1,
    'css_class' => '',
    'custom' => 0,
    'machine_name' => 'social_icons',
    'module' => 'block',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(),
    'themes' => array(
      'gladfagskole' => array(
        'region' => 'footer',
        'status' => 1,
        'theme' => 'gladfagskole',
        'weight' => 0,
      ),
    ),
    'title' => '',
    'visibility' => 0,
  );

  return $export;
}
