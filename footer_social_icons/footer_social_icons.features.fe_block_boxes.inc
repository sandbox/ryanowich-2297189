<?php
/**
 * @file
 * footer_social_icons.features.fe_block_boxes.inc
 */

/**
 * Implements hook_default_fe_block_boxes().
 */
function footer_social_icons_default_fe_block_boxes() {
  $export = array();

  $fe_block_boxes = new stdClass();
  $fe_block_boxes->info = 'Social icons';
  $fe_block_boxes->format = 'full_html';
  $fe_block_boxes->machine_name = 'social_icons';
  $fe_block_boxes->body = '<div class="socialicons"><a class="icon-button twitter" href="#"><i class="fa fa-twitter"><span class="hidden">Twitter</span></i></a> <a class="icon-button facebook" href="https://www.facebook.com/tvglad"><i class="fa fa-facebook"><span class="hidden">Facebook</span></i></a> <a class="icon-button google-plus" href="https://plus.google.com/118144288050600870299/about?hl=da-DK"><i class="fa fa-google-plus"><span class="hidden">Google Plus</span></i></a> <a class="icon-button youtube" href="http://www.youtube.com/tvglad"><i class="fa fa-youtube"><span class="hidden">YouTube</span></i></a> <a class="icon-button pinterest" href="#"><i class="fa fa-pinterest"><span class="hidden">Pinterest</span></i></a></div>
';

  $export['social_icons'] = $fe_block_boxes;

  return $export;
}
