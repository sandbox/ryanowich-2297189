<?php
/**
 * @file
 * slideshow_front.field_group.inc
 */

/**
 * Implements hook_field_group_info().
 */
function slideshow_front_field_group_info() {
  $export = array();

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_frontpage_slideshow|node|article|form';
  $field_group->group_name = 'group_frontpage_slideshow';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'article';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Frontpage Slideshow',
    'weight' => '4',
    'children' => array(
      0 => 'field_slideshow',
      1 => 'field_slideshow_image',
      2 => 'field_slideshow_image_crop',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'label' => 'Frontpage Slideshow',
      'instance_settings' => array(
        'required_fields' => 1,
        'classes' => 'group-frontpage-slideshow field-group-fieldset',
        'description' => '',
      ),
      'formatter' => 'collapsed',
    ),
  );
  $export['group_frontpage_slideshow|node|article|form'] = $field_group;

  return $export;
}
