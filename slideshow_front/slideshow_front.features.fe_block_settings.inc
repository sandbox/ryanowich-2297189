<?php
/**
 * @file
 * slideshow_front.features.fe_block_settings.inc
 */

/**
 * Implements hook_default_fe_block_settings().
 */
function slideshow_front_default_fe_block_settings() {
  $export = array();

  $export['version'] = '2.0';

  $export['views-slideshow_front_v2-slideshow'] = array(
    'cache' => -1,
    'css_class' => '',
    'custom' => 0,
    'delta' => 'slideshow_front_v2-slideshow',
    'module' => 'views',
    'node_types' => array(),
    'pages' => '<front>',
    'roles' => array(),
    'themes' => array(
      'gladfagskole' => array(
        'region' => 'featured',
        'status' => 1,
        'theme' => 'gladfagskole',
        'weight' => 0,
      ),
    ),
    'title' => '<none>',
    'visibility' => 1,
  );

  return $export;
}
