<?php
/**
 * @file
 * slideshow_front.views_default.inc
 */

/**
 * Implements hook_views_default_views().
 */
function slideshow_front_views_default_views() {
  $export = array();

  $view = new view();
  $view->name = 'slideshow_front_v2';
  $view->description = '';
  $view->tag = 'default';
  $view->base_table = 'node';
  $view->human_name = 'Slideshow Front';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['title'] = 'Slideshow (front)';
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'perm';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'none';
  $handler->display->display_options['style_plugin'] = 'list';
  $handler->display->display_options['style_options']['class'] = 'slideshow';
  $handler->display->display_options['style_options']['wrapper_class'] = 'slider';
  $handler->display->display_options['row_plugin'] = 'fields';
  /* Field: Content: Title */
  $handler->display->display_options['fields']['title']['id'] = 'title';
  $handler->display->display_options['fields']['title']['table'] = 'node';
  $handler->display->display_options['fields']['title']['field'] = 'title';
  $handler->display->display_options['fields']['title']['label'] = '';
  $handler->display->display_options['fields']['title']['alter']['word_boundary'] = FALSE;
  $handler->display->display_options['fields']['title']['alter']['ellipsis'] = FALSE;
  /* Sort criterion: Global: Random */
  $handler->display->display_options['sorts']['random']['id'] = 'random';
  $handler->display->display_options['sorts']['random']['table'] = 'views';
  $handler->display->display_options['sorts']['random']['field'] = 'random';
  /* Filter criterion: Content: Promoted to frontpage slideshow (field_slideshow) */
  $handler->display->display_options['filters']['field_slideshow_value']['id'] = 'field_slideshow_value';
  $handler->display->display_options['filters']['field_slideshow_value']['table'] = 'field_data_field_slideshow';
  $handler->display->display_options['filters']['field_slideshow_value']['field'] = 'field_slideshow_value';
  $handler->display->display_options['filters']['field_slideshow_value']['value'] = array(
    1 => '1',
  );
  $handler->display->display_options['filters']['field_slideshow_value']['group'] = 1;
  $handler->display->display_options['filters']['field_slideshow_value']['reduce_duplicates'] = TRUE;
  /* Filter criterion: Content: Slideshow image (field_slideshow_image_crop:fid) */
  $handler->display->display_options['filters']['field_slideshow_image_crop_fid']['id'] = 'field_slideshow_image_crop_fid';
  $handler->display->display_options['filters']['field_slideshow_image_crop_fid']['table'] = 'field_data_field_slideshow_image_crop';
  $handler->display->display_options['filters']['field_slideshow_image_crop_fid']['field'] = 'field_slideshow_image_crop_fid';
  $handler->display->display_options['filters']['field_slideshow_image_crop_fid']['operator'] = 'not empty';
  $handler->display->display_options['filters']['field_slideshow_image_crop_fid']['group'] = 1;
  /* Filter criterion: Content: Published */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'node';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = 1;
  $handler->display->display_options['filters']['status']['group'] = 1;
  $handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;

  /* Display: Slideshow */
  $handler = $view->new_display('block', 'Slideshow', 'slideshow');
  $handler->display->display_options['defaults']['css_class'] = FALSE;
  $handler->display->display_options['css_class'] = 'slideshow';
  $handler->display->display_options['defaults']['style_plugin'] = FALSE;
  $handler->display->display_options['style_plugin'] = 'default';
  $handler->display->display_options['style_options']['row_class'] = 'slide';
  $handler->display->display_options['style_options']['default_row_class'] = FALSE;
  $handler->display->display_options['style_options']['row_class_special'] = FALSE;
  $handler->display->display_options['defaults']['style_options'] = FALSE;
  $handler->display->display_options['defaults']['row_plugin'] = FALSE;
  $handler->display->display_options['row_plugin'] = 'fields';
  $handler->display->display_options['row_options']['hide_empty'] = TRUE;
  $handler->display->display_options['row_options']['default_field_elements'] = FALSE;
  $handler->display->display_options['defaults']['row_options'] = FALSE;
  $handler->display->display_options['defaults']['fields'] = FALSE;
  /* Field: Content: Slideshow image */
  $handler->display->display_options['fields']['field_slideshow_image_crop']['id'] = 'field_slideshow_image_crop';
  $handler->display->display_options['fields']['field_slideshow_image_crop']['table'] = 'field_data_field_slideshow_image_crop';
  $handler->display->display_options['fields']['field_slideshow_image_crop']['field'] = 'field_slideshow_image_crop';
  $handler->display->display_options['fields']['field_slideshow_image_crop']['label'] = '';
  $handler->display->display_options['fields']['field_slideshow_image_crop']['element_type'] = 'div';
  $handler->display->display_options['fields']['field_slideshow_image_crop']['element_class'] = 'slide_image';
  $handler->display->display_options['fields']['field_slideshow_image_crop']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_slideshow_image_crop']['element_default_classes'] = FALSE;
  $handler->display->display_options['fields']['field_slideshow_image_crop']['hide_empty'] = TRUE;
  $handler->display->display_options['fields']['field_slideshow_image_crop']['click_sort_column'] = 'fid';
  $handler->display->display_options['fields']['field_slideshow_image_crop']['settings'] = array(
    'image_style' => 'slideshow_image',
    'image_link' => 'content',
  );
  /* Field: Content: Title */
  $handler->display->display_options['fields']['title']['id'] = 'title';
  $handler->display->display_options['fields']['title']['table'] = 'node';
  $handler->display->display_options['fields']['title']['field'] = 'title';
  $handler->display->display_options['fields']['title']['label'] = '';
  $handler->display->display_options['fields']['title']['exclude'] = TRUE;
  $handler->display->display_options['fields']['title']['alter']['text'] = '<h2 class="slide_title">[title]</h2>';
  $handler->display->display_options['fields']['title']['alter']['nl2br'] = TRUE;
  $handler->display->display_options['fields']['title']['alter']['strip_tags'] = TRUE;
  $handler->display->display_options['fields']['title']['element_type'] = 'h2';
  $handler->display->display_options['fields']['title']['element_class'] = 'slide_title';
  $handler->display->display_options['fields']['title']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['title']['element_default_classes'] = FALSE;
  $handler->display->display_options['fields']['title']['hide_empty'] = TRUE;
  /* Field: Content: Body */
  $handler->display->display_options['fields']['body']['id'] = 'body';
  $handler->display->display_options['fields']['body']['table'] = 'field_data_body';
  $handler->display->display_options['fields']['body']['field'] = 'body';
  $handler->display->display_options['fields']['body']['label'] = '';
  $handler->display->display_options['fields']['body']['exclude'] = TRUE;
  $handler->display->display_options['fields']['body']['alter']['nl2br'] = TRUE;
  $handler->display->display_options['fields']['body']['alter']['strip_tags'] = TRUE;
  $handler->display->display_options['fields']['body']['element_type'] = 'p';
  $handler->display->display_options['fields']['body']['element_class'] = 'slide_body';
  $handler->display->display_options['fields']['body']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['body']['element_default_classes'] = FALSE;
  $handler->display->display_options['fields']['body']['hide_empty'] = TRUE;
  $handler->display->display_options['fields']['body']['type'] = 'text_summary_or_trimmed';
  $handler->display->display_options['fields']['body']['settings'] = array(
    'trim_length' => '200',
  );
  /* Field: Content: Path */
  $handler->display->display_options['fields']['path']['id'] = 'path';
  $handler->display->display_options['fields']['path']['table'] = 'node';
  $handler->display->display_options['fields']['path']['field'] = 'path';
  $handler->display->display_options['fields']['path']['label'] = '';
  $handler->display->display_options['fields']['path']['exclude'] = TRUE;
  $handler->display->display_options['fields']['path']['element_label_colon'] = FALSE;
  /* Field: Content: Title and body (rewrite) */
  $handler->display->display_options['fields']['counter']['id'] = 'counter';
  $handler->display->display_options['fields']['counter']['table'] = 'views';
  $handler->display->display_options['fields']['counter']['field'] = 'counter';
  $handler->display->display_options['fields']['counter']['ui_name'] = 'Content: Title and body (rewrite)';
  $handler->display->display_options['fields']['counter']['label'] = '';
  $handler->display->display_options['fields']['counter']['alter']['alter_text'] = TRUE;
  $handler->display->display_options['fields']['counter']['alter']['text'] = '<a href="[path]" class="slide_link"></a>
<div class="slide_text">
	<h2 class="slide_title">[title]</h2>
	<p class="slide_body">[body]</p>
</div>';
  $handler->display->display_options['fields']['counter']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['counter']['element_default_classes'] = FALSE;
  $handler->display->display_options['fields']['counter']['hide_empty'] = TRUE;
  $handler->display->display_options['fields']['counter']['counter_start'] = '1';
  $handler->display->display_options['defaults']['sorts'] = FALSE;
  /* Sort criterion: Global: Random */
  $handler->display->display_options['sorts']['random']['id'] = 'random';
  $handler->display->display_options['sorts']['random']['table'] = 'views';
  $handler->display->display_options['sorts']['random']['field'] = 'random';
  $translatables['slideshow_front_v2'] = array(
    t('Master'),
    t('Slideshow (front)'),
    t('more'),
    t('Apply'),
    t('Reset'),
    t('Sort by'),
    t('Asc'),
    t('Desc'),
    t('Slideshow'),
    t('<h2 class="slide_title">[title]</h2>'),
    t('<a href="[path]" class="slide_link"></a>
<div class="slide_text">
	<h2 class="slide_title">[title]</h2>
	<p class="slide_body">[body]</p>
</div>'),
  );
  $export['slideshow_front_v2'] = $view;

  return $export;
}
