<?php
/**
 * @file
 * content_types_fields_taxonomies.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function content_types_fields_taxonomies_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "field_group" && $api == "field_group") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_node_info().
 */
function content_types_fields_taxonomies_node_info() {
  $items = array(
    'article' => array(
      'name' => t('Article'),
      'base' => 'node_content',
      'description' => t('Teaser, tekst og 1 foto.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'every_field' => array(
      'name' => t('Every Field'),
      'base' => 'node_content',
      'description' => t('A content type with one of every field.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'gallery' => array(
      'name' => t('Gallery'),
      'base' => 'node_content',
      'description' => t('Et galleri indlæg, svarer i form til en artikel, er ideel til at skabe og vise indhold, der bærer en serie af billeder, som regel af fælles kontekst.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'kontakt' => array(
      'name' => t('Kontakt'),
      'base' => 'node_content',
      'description' => t('Vises på kontakt-siden.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  return $items;
}
