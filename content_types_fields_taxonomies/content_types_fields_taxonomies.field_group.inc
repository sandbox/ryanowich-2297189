<?php
/**
 * @file
 * content_types_fields_taxonomies.field_group.inc
 */

/**
 * Implements hook_field_group_info().
 */
function content_types_fields_taxonomies_field_group_info() {
  $export = array();

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_body_collapsed|node|gallery|form';
  $field_group->group_name = 'group_body_collapsed';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'gallery';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Body',
    'weight' => '1',
    'children' => array(
      0 => 'body',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'label' => 'Body',
      'instance_settings' => array(
        'required_fields' => 1,
        'classes' => 'group-body-collapsed field-group-fieldset',
        'description' => '',
      ),
      'formatter' => 'collapsed',
    ),
  );
  $export['group_body_collapsed|node|gallery|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_extra_addressinfo|node|kontakt|default';
  $field_group->group_name = 'group_extra_addressinfo';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'kontakt';
  $field_group->mode = 'default';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Ekstra adresseinfo',
    'weight' => '1',
    'children' => array(
      0 => 'field_google_maps_adress',
      1 => 'every_field_text_long',
    ),
    'format_type' => 'html5',
    'format_settings' => array(
      'label' => 'Ekstra adresseinfo',
      'instance_settings' => array(
        'classes' => 'extra-addressinfo clearfix',
        'wrapper' => 'section',
      ),
    ),
  );
  $export['group_extra_addressinfo|node|kontakt|default'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_extra_addressinfo|node|kontakt|form';
  $field_group->group_name = 'group_extra_addressinfo';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'kontakt';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Ekstra adresseinfo',
    'weight' => '2',
    'children' => array(
      0 => 'field_google_maps_adress',
      1 => 'every_field_text_long',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'label' => 'Ekstra adresseinfo',
      'instance_settings' => array(
        'required_fields' => 1,
        'classes' => 'group-extra-addressinfo field-group-fieldset',
        'description' => '',
      ),
      'formatter' => 'collapsed',
    ),
  );
  $export['group_extra_addressinfo|node|kontakt|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_frontpage_slideshow|node|gallery|form';
  $field_group->group_name = 'group_frontpage_slideshow';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'gallery';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Frontpage Slideshow',
    'weight' => '5',
    'children' => array(
      0 => 'field_slideshow',
      1 => 'field_slideshow_image',
      2 => 'field_slideshow_image_crop',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'label' => 'Frontpage Slideshow',
      'instance_settings' => array(
        'required_fields' => 1,
        'classes' => 'group-frontpage-slideshow field-group-fieldset',
        'description' => '',
      ),
      'formatter' => 'collapsed',
    ),
  );
  $export['group_frontpage_slideshow|node|gallery|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_gallery_images|node|gallery|form';
  $field_group->group_name = 'group_gallery_images';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'gallery';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Gallery images',
    'weight' => '3',
    'children' => array(
      0 => 'field_gallery_image',
      1 => 'field_gallery_image_item',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'formatter' => 'collapsible',
      'instance_settings' => array(
        'description' => '',
        'classes' => 'group-gallery-images field-group-fieldset',
        'required_fields' => 1,
      ),
    ),
  );
  $export['group_gallery_images|node|gallery|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_kontaktperson|field_collection_item|field_kontaktperson|default';
  $field_group->group_name = 'group_kontaktperson';
  $field_group->entity_type = 'field_collection_item';
  $field_group->bundle = 'field_kontaktperson';
  $field_group->mode = 'default';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Kontaktperson',
    'weight' => '0',
    'children' => array(
      0 => 'field_image',
      1 => 'field_navn',
      2 => 'field_titel',
      3 => 'field_telefon',
      4 => 'field_e_mail',
    ),
    'format_type' => 'html5',
    'format_settings' => array(
      'label' => 'Kontaktperson',
      'instance_settings' => array(
        'classes' => 'kontaktperson clearfix',
        'wrapper' => 'section',
      ),
    ),
  );
  $export['group_kontaktperson|field_collection_item|field_kontaktperson|default'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_kontaktperson|field_collection_item|field_kontaktperson|form';
  $field_group->group_name = 'group_kontaktperson';
  $field_group->entity_type = 'field_collection_item';
  $field_group->bundle = 'field_kontaktperson';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Kontaktperson',
    'weight' => '0',
    'children' => array(
      0 => 'field_image',
      1 => 'field_navn',
      2 => 'field_titel',
      3 => 'field_telefon',
      4 => 'field_e_mail',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'label' => 'Kontaktperson',
      'instance_settings' => array(
        'required_fields' => 1,
        'classes' => 'group-kontaktperson field-group-fieldset',
        'description' => '',
      ),
      'formatter' => 'collapsed',
    ),
  );
  $export['group_kontaktperson|field_collection_item|field_kontaktperson|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_organize|node|article|form';
  $field_group->group_name = 'group_organize';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'article';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Organisering',
    'weight' => '3',
    'children' => array(
      0 => 'field_tags',
      1 => 'field_menuemne',
      2 => 'field_nyhed',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'label' => 'Organisering',
      'instance_settings' => array(
        'required_fields' => 1,
        'classes' => 'group-organize field-group-fieldset',
        'description' => '',
      ),
      'formatter' => 'collapsed',
    ),
  );
  $export['group_organize|node|article|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_organize|node|gallery|form';
  $field_group->group_name = 'group_organize';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'gallery';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Organisering',
    'weight' => '4',
    'children' => array(
      0 => 'field_tags',
      1 => 'field_menuemne',
      2 => 'field_nyhed',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'label' => 'Organisering',
      'instance_settings' => array(
        'required_fields' => 1,
        'classes' => 'group-organize field-group-fieldset',
        'description' => '',
      ),
      'formatter' => 'collapsed',
    ),
  );
  $export['group_organize|node|gallery|form'] = $field_group;

  return $export;
}
