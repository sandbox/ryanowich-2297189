<?php
/**
 * @file
 * frontpage.features.inc
 */

/**
 * Implements hook_views_api().
 */
function frontpage_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}
