<?php
/**
 * @file
 * menu_info_glad_l_ring_faglinjer_aktuelt_kontakt.features.menu_links.inc
 */

/**
 * Implements hook_menu_default_menu_links().
 */
function menu_info_glad_l_ring_faglinjer_aktuelt_kontakt_menu_default_menu_links() {
  $menu_links = array();

  // Exported menu link: main-menu_aktuelt:aktuelt
  $menu_links['main-menu_aktuelt:aktuelt'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'aktuelt',
    'router_path' => 'aktuelt',
    'link_title' => 'Aktuelt',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'main-menu_aktuelt:aktuelt',
    ),
    'module' => 'system',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -46,
    'customized' => 1,
  );
  // Exported menu link: main-menu_faglinjer:faglinjer
  $menu_links['main-menu_faglinjer:faglinjer'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'faglinjer',
    'router_path' => 'faglinjer',
    'link_title' => 'Faglinjer',
    'options' => array(
      'attributes' => array(
        'title' => 'Oversigt over faglinier',
      ),
      'identifier' => 'main-menu_faglinjer:faglinjer',
    ),
    'module' => 'system',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 1,
    'expanded' => 1,
    'weight' => -47,
    'customized' => 1,
  );
  // Exported menu link: main-menu_glad-lring:taxonomy/term/18
  $menu_links['main-menu_glad-lring:taxonomy/term/18'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'taxonomy/term/18',
    'router_path' => 'taxonomy/term/%',
    'link_title' => 'Glad læring',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'main-menu_glad-lring:taxonomy/term/18',
    ),
    'module' => 'taxonomy_menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -49,
    'customized' => 1,
  );
  // Exported menu link: main-menu_info:taxonomy/term/22
  $menu_links['main-menu_info:taxonomy/term/22'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'taxonomy/term/22',
    'router_path' => 'taxonomy/term/%',
    'link_title' => 'Info',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'main-menu_info:taxonomy/term/22',
    ),
    'module' => 'taxonomy_menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -50,
    'customized' => 1,
  );
  // Exported menu link: main-menu_kontakt:node/146
  $menu_links['main-menu_kontakt:node/146'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'node/146',
    'router_path' => 'node/%',
    'link_title' => 'Kontakt',
    'options' => array(
      'identifier' => 'main-menu_kontakt:node/146',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -45,
    'customized' => 1,
  );
  // Translatables
  // Included for use with string extractors like potx.
  t('Aktuelt');
  t('Faglinjer');
  t('Glad læring');
  t('Info');
  t('Kontakt');


  return $menu_links;
}
